<?php
App::uses('MembershipAppController', 'Membership.Controller');
/**
 * Deductions Controller
 *
 * @property Deduction $Deduction
 * @property PaginatorComponent $Paginator
 */
class DeductionsController extends MembershipAppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Search.Prg');

/**
 * index method
 *
 * @return void
 */
	public function index() {

		$this->Prg->commonProcess();
		$this->Paginator->settings['conditions'] = $this->Deduction->parseCriteria($this->Prg->parsedParams());
		$this->set('deductions', $this->Paginator->paginate());

		// deprecated - suhaimi
		// $this->Deduction->recursive = 0;
		// $this->set('deductions', $this->Paginator->paginate());
	}

	public function ends_with($haystack, $needle)
	{
	    $length = strlen($needle);
	    if ($length == 0) {
	        return true;
	    }

	    return (substr($haystack, -$length) === $needle);
	}
/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Deduction->exists($id)) {
			throw new NotFoundException(__('Invalid deduction'));
		}
		$options = array('conditions' => array('Deduction.' . $this->Deduction->primaryKey => $id));
		$this->set('deduction', $this->Deduction->find('first', $options));
	}

	public function object($id = null) {
		
		$options = array('conditions' => array('Deduction.' . $this->Deduction->primaryKey => $id));
		return $this->Deduction->find('first', $options);
	}

/**
 * sneak method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function sneak($id = null) {
		if (!$this->Deduction->exists($id)) {
			throw new NotFoundException(__('Invalid deduction'));
		}
		$options = array('conditions' => array('Deduction.' . $this->Deduction->primaryKey => $id));
		$this->set('deduction', $this->Deduction->find('first', $options));
	}

/**
 * calendar method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function calendar($id = null) {
		if (!$this->Deduction->exists($id)) {
			throw new NotFoundException(__('Invalid deduction'));
		}
		$options = array('conditions' => array('Deduction.' . $this->Deduction->primaryKey => $id));
		$this->set('deduction', $this->Deduction->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			if (isset($this->request->data['Deduction']['start_date'])) {
				$this->request->data['Deduction']['start_date'] = $this->split_date($this->request->data['Deduction']['start_date']);
				$this->request->data['Deduction']['end_date'] = $this->split_date($this->request->data['Deduction']['end_date']);
			}
			

			$this->Deduction->create();
			if ($this->Deduction->save($this->request->data)) {
				$this->Session->setFlash(__('The deduction has been saved.'), 'default', array('class' => 'alert alert-success'));

				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The deduction could not be saved. Please, try again.'), 'default', array('class' => 'alert alert-danger'));
			}
		}
		$users = $this->Deduction->User->find('list');
		$members = $this->Deduction->Member->find('list');
		$this->set(compact('users', 'members'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Deduction->exists($id)) {
			throw new NotFoundException(__('Invalid deduction'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if (isset($this->request->data['Deduction']['start_date'])) {
				$this->request->data['Deduction']['start_date'] = $this->split_date($this->request->data['Deduction']['start_date']);
				$this->request->data['Deduction']['end_date'] = $this->split_date($this->request->data['Deduction']['end_date']);
			}
			
			if ($this->Deduction->save($this->request->data)) {
				$this->Session->setFlash(__('The deduction has been saved.'), 'default', array('class' => 'alert alert-success'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The deduction could not be saved. Please, try again.'), 'default', array('class' => 'alert alert-danger'));
			}
		} else {
			$options = array('conditions' => array('Deduction.' . $this->Deduction->primaryKey => $id));
			$this->request->data = $this->Deduction->find('first', $options);
		}
		$users = $this->Deduction->User->find('list');
		$members = $this->Deduction->Member->find('list');
		$this->set(compact('users', 'members'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Deduction->id = $id;
		if (!$this->Deduction->exists()) {
			throw new NotFoundException(__('Invalid deduction'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Deduction->delete()) {
			$this->Session->setFlash(__('The deduction has been deleted.'), 'default', array('class' => 'alert alert-success'));
		} else {
			$this->Session->setFlash(__('The deduction could not be deleted. Please, try again.'), 'default', array('class' => 'alert alert-danger'));
		}
		return $this->redirect(array('action' => 'index'));
	}


/**
 * split_date method
 *
 * @return array 
 */
	public function split_date($input) {
		$arr = explode("-", $input);
	   
		//Display the Start Date array format
		return array(
			 "day" => $arr[0], 
			 "month" => $arr[1], 
			 "year" => $arr[2]
		);
	}



    function end_with($haystack, $needle)
  	{
	    $length = strlen($needle);
	    if ($length == 0) {
	        return true;
	    }

      	return (substr($haystack, -$length) === $needle);
  	}

}
