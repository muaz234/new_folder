<?php
App::uses('MembershipAppController', 'Membership.Controller');
/**
 * WelfareTypes Controller
 *
 * @property WelfareType $WelfareType
 * @property PaginatorComponent $Paginator
 */
class WelfareTypesController extends MembershipAppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Search.Prg');

/**
 * index method
 *
 * @return void
 */
	public function index() {

		$this->Prg->commonProcess();
		$this->Paginator->settings['conditions'] = $this->WelfareType->parseCriteria($this->Prg->parsedParams());
		$this->set('welfareTypes', $this->Paginator->paginate());

		// deprecated - suhaimi
		// $this->WelfareType->recursive = 0;
		// $this->set('welfareTypes', $this->Paginator->paginate());
	}

	public function ends_with($haystack, $needle)
	{
	    $length = strlen($needle);
	    if ($length == 0) {
	        return true;
	    }

	    return (substr($haystack, -$length) === $needle);
	}
/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->WelfareType->exists($id)) {
			throw new NotFoundException(__('Invalid welfare type'));
		}
		$options = array('conditions' => array('WelfareType.' . $this->WelfareType->primaryKey => $id));
		$this->set('welfareType', $this->WelfareType->find('first', $options));
	}

	public function object($id = null) {
		
		$options = array('conditions' => array('WelfareType.' . $this->WelfareType->primaryKey => $id));
		return $this->WelfareType->find('first', $options);
	}

/**
 * sneak method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function sneak($id = null) {
		if (!$this->WelfareType->exists($id)) {
			throw new NotFoundException(__('Invalid welfare type'));
		}
		$options = array('conditions' => array('WelfareType.' . $this->WelfareType->primaryKey => $id));
		$this->set('welfareType', $this->WelfareType->find('first', $options));
	}

/**
 * calendar method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function calendar($id = null) {
		if (!$this->WelfareType->exists($id)) {
			throw new NotFoundException(__('Invalid welfare type'));
		}
		$options = array('conditions' => array('WelfareType.' . $this->WelfareType->primaryKey => $id));
		$this->set('welfareType', $this->WelfareType->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			if (isset($this->request->data['WelfareType']['start_date'])) {
				$this->request->data['WelfareType']['start_date'] = $this->split_date($this->request->data['WelfareType']['start_date']);
				$this->request->data['WelfareType']['end_date'] = $this->split_date($this->request->data['WelfareType']['end_date']);
			}
			

			$this->WelfareType->create();
			if ($this->WelfareType->save($this->request->data)) {
				$this->Session->setFlash(__('The welfare type has been saved.'), 'default', array('class' => 'alert alert-success'));

				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The welfare type could not be saved. Please, try again.'), 'default', array('class' => 'alert alert-danger'));
			}
		}
		$users = $this->WelfareType->User->find('list');
		$this->set(compact('users'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->WelfareType->exists($id)) {
			throw new NotFoundException(__('Invalid welfare type'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if (isset($this->request->data['WelfareType']['start_date'])) {
				$this->request->data['WelfareType']['start_date'] = $this->split_date($this->request->data['WelfareType']['start_date']);
				$this->request->data['WelfareType']['end_date'] = $this->split_date($this->request->data['WelfareType']['end_date']);
			}
			
			if ($this->WelfareType->save($this->request->data)) {
				$this->Session->setFlash(__('The welfare type has been saved.'), 'default', array('class' => 'alert alert-success'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The welfare type could not be saved. Please, try again.'), 'default', array('class' => 'alert alert-danger'));
			}
		} else {
			$options = array('conditions' => array('WelfareType.' . $this->WelfareType->primaryKey => $id));
			$this->request->data = $this->WelfareType->find('first', $options);
		}
		$users = $this->WelfareType->User->find('list');
		$this->set(compact('users'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->WelfareType->id = $id;
		if (!$this->WelfareType->exists()) {
			throw new NotFoundException(__('Invalid welfare type'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->WelfareType->delete()) {
			$this->Session->setFlash(__('The welfare type has been deleted.'), 'default', array('class' => 'alert alert-success'));
		} else {
			$this->Session->setFlash(__('The welfare type could not be deleted. Please, try again.'), 'default', array('class' => 'alert alert-danger'));
		}
		return $this->redirect(array('action' => 'index'));
	}


/**
 * split_date method
 *
 * @return array 
 */
	public function split_date($input) {
		$arr = explode("-", $input);
	   
		//Display the Start Date array format
		return array(
			 "day" => $arr[0], 
			 "month" => $arr[1], 
			 "year" => $arr[2]
		);
	}



    function end_with($haystack, $needle)
  	{
	    $length = strlen($needle);
	    if ($length == 0) {
	        return true;
	    }

      	return (substr($haystack, -$length) === $needle);
  	}

}
