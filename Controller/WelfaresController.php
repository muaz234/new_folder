<?php
App::uses('MembershipAppController', 'Membership.Controller');
/**
 * Welfares Controller
 *
 * @property Welfare $Welfare
 * @property PaginatorComponent $Paginator
 */
class WelfaresController extends MembershipAppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Search.Prg');

/**
 * index method
 *
 * @return void
 */
	public function index() {

		$this->Prg->commonProcess();
		$this->Paginator->settings['conditions'] = $this->Welfare->parseCriteria($this->Prg->parsedParams());
		$this->set('welfares', $this->Paginator->paginate());

		// deprecated - suhaimi
		// $this->Welfare->recursive = 0;
		// $this->set('welfares', $this->Paginator->paginate());
	}

	public function ends_with($haystack, $needle)
	{
	    $length = strlen($needle);
	    if ($length == 0) {
	        return true;
	    }

	    return (substr($haystack, -$length) === $needle);
	}
/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Welfare->exists($id)) {
			throw new NotFoundException(__('Invalid welfare'));
		}
		$options = array('conditions' => array('Welfare.' . $this->Welfare->primaryKey => $id));
		$this->set('welfare', $this->Welfare->find('first', $options));
	}

	public function object($id = null) {
		
		$options = array('conditions' => array('Welfare.' . $this->Welfare->primaryKey => $id));
		return $this->Welfare->find('first', $options);
	}

/**
 * sneak method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function sneak($id = null) {
		if (!$this->Welfare->exists($id)) {
			throw new NotFoundException(__('Invalid welfare'));
		}
		$options = array('conditions' => array('Welfare.' . $this->Welfare->primaryKey => $id));
		$this->set('welfare', $this->Welfare->find('first', $options));
	}

/**
 * calendar method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function calendar($id = null) {
		if (!$this->Welfare->exists($id)) {
			throw new NotFoundException(__('Invalid welfare'));
		}
		$options = array('conditions' => array('Welfare.' . $this->Welfare->primaryKey => $id));
		$this->set('welfare', $this->Welfare->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			if (isset($this->request->data['Welfare']['start_date'])) {
				$this->request->data['Welfare']['start_date'] = $this->split_date($this->request->data['Welfare']['start_date']);
				$this->request->data['Welfare']['end_date'] = $this->split_date($this->request->data['Welfare']['end_date']);
			}
			

			$this->Welfare->create();
			if ($this->Welfare->save($this->request->data)) {
				$this->Session->setFlash(__('The welfare has been saved.'), 'default', array('class' => 'alert alert-success'));

				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The welfare could not be saved. Please, try again.'), 'default', array('class' => 'alert alert-danger'));
			}
		}
		$users = $this->Welfare->User->find('list');
		$members = $this->Welfare->Member->find('list');
		$welfareTypes = $this->Welfare->WelfareType->find('list');
		$this->set(compact('users', 'members', 'welfareTypes'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Welfare->exists($id)) {
			throw new NotFoundException(__('Invalid welfare'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if (isset($this->request->data['Welfare']['start_date'])) {
				$this->request->data['Welfare']['start_date'] = $this->split_date($this->request->data['Welfare']['start_date']);
				$this->request->data['Welfare']['end_date'] = $this->split_date($this->request->data['Welfare']['end_date']);
			}
			
			if ($this->Welfare->save($this->request->data)) {
				$this->Session->setFlash(__('The welfare has been saved.'), 'default', array('class' => 'alert alert-success'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The welfare could not be saved. Please, try again.'), 'default', array('class' => 'alert alert-danger'));
			}
		} else {
			$options = array('conditions' => array('Welfare.' . $this->Welfare->primaryKey => $id));
			$this->request->data = $this->Welfare->find('first', $options);
		}
		$users = $this->Welfare->User->find('list');
		$members = $this->Welfare->Member->find('list');
		$welfareTypes = $this->Welfare->WelfareType->find('list');
		$this->set(compact('users', 'members', 'welfareTypes'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Welfare->id = $id;
		if (!$this->Welfare->exists()) {
			throw new NotFoundException(__('Invalid welfare'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Welfare->delete()) {
			$this->Session->setFlash(__('The welfare has been deleted.'), 'default', array('class' => 'alert alert-success'));
		} else {
			$this->Session->setFlash(__('The welfare could not be deleted. Please, try again.'), 'default', array('class' => 'alert alert-danger'));
		}
		return $this->redirect(array('action' => 'index'));
	}


/**
 * split_date method
 *
 * @return array 
 */
	public function split_date($input) {
		$arr = explode("-", $input);
	   
		//Display the Start Date array format
		return array(
			 "day" => $arr[0], 
			 "month" => $arr[1], 
			 "year" => $arr[2]
		);
	}



    function end_with($haystack, $needle)
  	{
	    $length = strlen($needle);
	    if ($length == 0) {
	        return true;
	    }

      	return (substr($haystack, -$length) === $needle);
  	}

}
