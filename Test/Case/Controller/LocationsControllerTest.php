<?php
App::uses('LocationsController', 'Membership.Controller');

/**
 * LocationsController Test Case
 *
 */
class LocationsControllerTest extends ControllerTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'plugin.membership.location',
		'plugin.membership.member'
	);

/**
 * testIndex method
 *
 * @return void
 */
	public function testIndex() {
		$this->markTestIncomplete('testIndex not implemented.');
	}

/**
 * testEndsWith method
 *
 * @return void
 */
	public function testEndsWith() {
		$this->markTestIncomplete('testEndsWith not implemented.');
	}

/**
 * testView method
 *
 * @return void
 */
	public function testView() {
		$this->markTestIncomplete('testView not implemented.');
	}

/**
 * testObject method
 *
 * @return void
 */
	public function testObject() {
		$this->markTestIncomplete('testObject not implemented.');
	}

/**
 * testSneak method
 *
 * @return void
 */
	public function testSneak() {
		$this->markTestIncomplete('testSneak not implemented.');
	}

/**
 * testCalendar method
 *
 * @return void
 */
	public function testCalendar() {
		$this->markTestIncomplete('testCalendar not implemented.');
	}

/**
 * testAdd method
 *
 * @return void
 */
	public function testAdd() {
		$this->markTestIncomplete('testAdd not implemented.');
	}

/**
 * testEdit method
 *
 * @return void
 */
	public function testEdit() {
		$this->markTestIncomplete('testEdit not implemented.');
	}

/**
 * testDelete method
 *
 * @return void
 */
	public function testDelete() {
		$this->markTestIncomplete('testDelete not implemented.');
	}

/**
 * testSplitDate method
 *
 * @return void
 */
	public function testSplitDate() {
		$this->markTestIncomplete('testSplitDate not implemented.');
	}

/**
 * testEndWith method
 *
 * @return void
 */
	public function testEndWith() {
		$this->markTestIncomplete('testEndWith not implemented.');
	}

}
