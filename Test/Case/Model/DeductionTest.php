<?php
App::uses('Deduction', 'Membership.Model');

/**
 * Deduction Test Case
 *
 */
class DeductionTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'plugin.membership.deduction',
		'plugin.membership.user',
		'plugin.membership.member'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Deduction = ClassRegistry::init('Membership.Deduction');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Deduction);

		parent::tearDown();
	}

}
