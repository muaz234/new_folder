<?php
App::uses('Member', 'Membership.Model');

/**
 * Member Test Case
 *
 */
class MemberTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'plugin.membership.member',
		'plugin.membership.location',
		'plugin.membership.user',
		'plugin.membership.deduction',
		'plugin.membership.welfare'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Member = ClassRegistry::init('Membership.Member');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Member);

		parent::tearDown();
	}

}
