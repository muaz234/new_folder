<?php
App::uses('Welfare', 'Membership.Model');

/**
 * Welfare Test Case
 *
 */
class WelfareTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'plugin.membership.welfare',
		'plugin.membership.user',
		'plugin.membership.member',
		'plugin.membership.welfare_type'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Welfare = ClassRegistry::init('Membership.Welfare');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Welfare);

		parent::tearDown();
	}

}
