<?php
App::uses('WelfareType', 'Membership.Model');

/**
 * WelfareType Test Case
 *
 */
class WelfareTypeTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'plugin.membership.welfare_type',
		'plugin.membership.user',
		'plugin.membership.welfare'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->WelfareType = ClassRegistry::init('Membership.WelfareType');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->WelfareType);

		parent::tearDown();
	}

}
