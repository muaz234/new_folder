<?php
/**
 * MemberFixture
 *
 */
class MemberFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'primary'),
		'location_id' => array('type' => 'integer', 'null' => true, 'default' => null, 'unsigned' => false),
		'register_date' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 16, 'collate' => 'utf8mb4_unicode_ci', 'charset' => 'utf8mb4'),
		'register_by' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 25, 'collate' => 'utf8mb4_unicode_ci', 'charset' => 'utf8mb4'),
		'fullname' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 47, 'collate' => 'utf8mb4_unicode_ci', 'charset' => 'utf8mb4'),
		'ic_no' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 12, 'collate' => 'utf8mb4_unicode_ci', 'charset' => 'utf8mb4'),
		'staff_no' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 8, 'collate' => 'utf8mb4_unicode_ci', 'charset' => 'utf8mb4'),
		'department' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 42, 'collate' => 'utf8mb4_unicode_ci', 'charset' => 'utf8mb4'),
		'location' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 16, 'collate' => 'utf8mb4_unicode_ci', 'charset' => 'utf8mb4'),
		'address' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 202, 'collate' => 'utf8mb4_unicode_ci', 'charset' => 'utf8mb4'),
		'phone_no' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 17, 'collate' => 'utf8mb4_unicode_ci', 'charset' => 'utf8mb4'),
		'membership_no' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 4, 'collate' => 'utf8mb4_unicode_ci', 'charset' => 'utf8mb4'),
		'old_form_no' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 4, 'collate' => 'utf8mb4_unicode_ci', 'charset' => 'utf8mb4'),
		'status' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 10, 'collate' => 'utf8mb4_unicode_ci', 'charset' => 'utf8mb4'),
		'not_active_date' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 15, 'collate' => 'utf8mb4_unicode_ci', 'charset' => 'utf8mb4'),
		'received_form' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 3, 'collate' => 'utf8mb4_unicode_ci', 'charset' => 'utf8mb4'),
		'remarks' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 152, 'collate' => 'utf8mb4_unicode_ci', 'charset' => 'utf8mb4'),
		'user_id' => array('type' => 'integer', 'null' => true, 'default' => null, 'unsigned' => false),
		'created' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'updated' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'utf8mb4', 'collate' => 'utf8mb4_unicode_ci', 'engine' => 'InnoDB', 'comment' => 'stores all BAKIP membership')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'location_id' => 1,
			'register_date' => 'Lorem ipsum do',
			'register_by' => 'Lorem ipsum dolor sit a',
			'fullname' => 'Lorem ipsum dolor sit amet',
			'ic_no' => 'Lorem ipsu',
			'staff_no' => 'Lorem ',
			'department' => 'Lorem ipsum dolor sit amet',
			'location' => 'Lorem ipsum do',
			'address' => 'Lorem ipsum dolor sit amet',
			'phone_no' => 'Lorem ipsum dol',
			'membership_no' => 'Lo',
			'old_form_no' => 'Lo',
			'status' => 'Lorem ip',
			'not_active_date' => 'Lorem ipsum d',
			'received_form' => 'L',
			'remarks' => 'Lorem ipsum dolor sit amet',
			'user_id' => 1,
			'created' => '2018-07-04 10:50:45',
			'updated' => '2018-07-04 10:50:45'
		),
	);

}
