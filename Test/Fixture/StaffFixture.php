<?php
/**
 * StaffFixture
 *
 */
class StaffFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 4, 'unsigned' => false, 'key' => 'primary'),
		'staff_number' => array('type' => 'integer', 'null' => true, 'default' => null, 'length' => 8, 'unsigned' => false),
		'Staff_Name' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 75, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'Company' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 30, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'Depot' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 16, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'Working_Type' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 19, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'Group_Level' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 20, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'Employment_Status' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 17, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'Entry_Date' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 10, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'Seniority_Date' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 14, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'Leaving_Date' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 12, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'Years_of_Service' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 26, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'Organizational_Unit' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 40, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'Cost_Center' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 20, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'Cost_Center_Id' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 14, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'Designation' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 39, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'Division' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 39, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'Department' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 40, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'Section' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 40, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'Unit' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 40, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'Subunit' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 40, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'Staff_Grade' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 14, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'Gender' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 6, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'Date_of_Birth' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 13, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'Age' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 15, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'Remarks' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 27, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_general_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'staff_number' => 1,
			'Staff_Name' => 'Lorem ipsum dolor sit amet',
			'Company' => 'Lorem ipsum dolor sit amet',
			'Depot' => 'Lorem ipsum do',
			'Working_Type' => 'Lorem ipsum dolor',
			'Group_Level' => 'Lorem ipsum dolor ',
			'Employment_Status' => 'Lorem ipsum dol',
			'Entry_Date' => 'Lorem ip',
			'Seniority_Date' => 'Lorem ipsum ',
			'Leaving_Date' => 'Lorem ipsu',
			'Years_of_Service' => 'Lorem ipsum dolor sit am',
			'Organizational_Unit' => 'Lorem ipsum dolor sit amet',
			'Cost_Center' => 'Lorem ipsum dolor ',
			'Cost_Center_Id' => 'Lorem ipsum ',
			'Designation' => 'Lorem ipsum dolor sit amet',
			'Division' => 'Lorem ipsum dolor sit amet',
			'Department' => 'Lorem ipsum dolor sit amet',
			'Section' => 'Lorem ipsum dolor sit amet',
			'Unit' => 'Lorem ipsum dolor sit amet',
			'Subunit' => 'Lorem ipsum dolor sit amet',
			'Staff_Grade' => 'Lorem ipsum ',
			'Gender' => 'Lore',
			'Date_of_Birth' => 'Lorem ipsum',
			'Age' => 'Lorem ipsum d',
			'Remarks' => 'Lorem ipsum dolor sit ame'
		),
	);

}
