<?php
/**
 * WelfareFixture
 *
 */
class WelfareFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'primary'),
		'user_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false),
		'member_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false),
		'welfare_type_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false),
		'approved_on' => array('type' => 'date', 'null' => true, 'default' => null),
		'issuance_date' => array('type' => 'date', 'null' => true, 'default' => null),
		'created' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'updated' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_unicode_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'user_id' => 1,
			'member_id' => 1,
			'welfare_type_id' => 1,
			'approved_on' => '2018-07-04',
			'issuance_date' => '2018-07-04',
			'created' => '2018-07-04 10:50:48',
			'updated' => '2018-07-04 10:50:48'
		),
	);

}
