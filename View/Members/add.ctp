

<div class="modal-dialog">
  <div class="modal-content">
  	<?php echo $this->Form->create('Member', array('type' => 'file', 'class' => 'bs-example form-horizontal')); ?>
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal">&times;</button>
      <h4 class="modal-title"><?php echo __('Add Member'); ?></h4>
    </div>
    <div class="modal-body">
				<?php
		echo $this->Form->input('location_id', array('class' => 'form-control'));
		echo $this->Form->input('register_date', array('class' => 'form-control'));
		echo $this->Form->input('register_by', array('class' => 'form-control'));
		echo $this->Form->input('fullname', array('class' => 'form-control'));
		echo $this->Form->input('ic_no', array('class' => 'form-control'));
		echo $this->Form->input('staff_no', array('class' => 'form-control'));
		echo $this->Form->input('department', array('class' => 'form-control'));
		echo $this->Form->input('location', array('class' => 'form-control'));
		echo $this->Form->input('address', array('class' => 'form-control'));
		echo $this->Form->input('phone_no', array('class' => 'form-control'));
		echo $this->Form->input('membership_no', array('class' => 'form-control'));
		echo $this->Form->input('old_form_no', array('class' => 'form-control'));
		echo $this->Form->input('status', array('class' => 'form-control'));
		echo $this->Form->input('not_active_date', array('class' => 'form-control'));
		echo $this->Form->input('received_form', array('class' => 'form-control'));
		echo $this->Form->input('remarks', array('class' => 'form-control'));
		echo $this->Form->input('user_id', array('class' => 'form-control'));
	?>
	</div>
    <div class="modal-footer">
    	<?php echo $this->Layout->sessionFlash(); ?>
    	<a href="#" class="btn btn-default" data-dismiss="modal">Close</a>
    	<?php echo $this->Form->button('Submit', array('class' => 'btn btn-primary'));?>
    </div>
    	<?php echo $this->Form->end();?>
  </div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->


