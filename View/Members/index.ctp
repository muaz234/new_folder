          <ul class="breadcrumb no-border no-radius b-b b-light pull-in">
            <li><a href="<?php echo $this->webroot; ?>"<i class="fa fa-home"></i> Home</a></li>
            <li class="active"><?php echo __('Member'); ?> List</li>
          </ul>
          <div class="m-b-md">
            <h3 class="m-b-none"><?php echo __('Member'); ?> List</h3>
          </div>

              <section class="panel panel-default">
                <div class="row wrapper">
                  <div class="col-sm-5 m-b-xs">
                      <div class="btn-group">
                        <button type="button" class="btn btn-sm btn-default" title="Refresh" onclick ='location.reload();'><i class="fa fa-refresh"></i></button>
                      </div>
                       			<?php echo $this->Html->link(' Create', array('action' => 'add'), array('class' => 'btn btn-success fa fa-plus', 'data-toggle'=>'ajaxModal', 'escape' => false)); ?>
 
                  </div>
                  <div class="col-sm-4 m-b-xs">
                    &nbsp;
                  </div>
                  <div class="col-sm-3">
                      <?php echo $this->Form->create('Member', array('class' => 'bs-example form-horizontal', 'inputDefaults' => array('label' => false, 'div' => false))); ?>
                      <div class="input-group">
                      	<?php
		echo $this->Form->input('queryString', array('class' => 'input-sm form-control', 'placeholder' => 'Search')); ?>
<span class='input-group-btn'>
<?php echo $this->Form->button('Go!', array('class' => 'btn btn-sm btn-default bg-success'));?>
</span>
		</div><?php echo $this->Form->end();?>
                    </div>
                  </div>
                </div>
                <div class="table-responsive">
                  <table class="table table-striped b-t b-light">
                    <thead>
                      <tr>
                        
                        <th class="col-md-2 text-center sorting"><?php echo __('Actions'); ?></th>
                                                        					                                  						<th>
            							 <?php echo $this->Paginator->sort('location_id'); ?>            						</th>
                                    					                                  						<th>
            							 <?php echo $this->Paginator->sort('register_date'); ?>            						</th>
                                    					                                  						<th>
            							 <?php echo $this->Paginator->sort('register_by'); ?>            						</th>
                                    					                                  						<th>
            							 <?php echo $this->Paginator->sort('fullname'); ?>            						</th>
                                    					                                  						<th>
            							 <?php echo $this->Paginator->sort('ic_no'); ?>            						</th>
                                    					                                  						<th>
            							 <?php echo $this->Paginator->sort('staff_no'); ?>            						</th>
                                    					                                  						<th>
            							 <?php echo $this->Paginator->sort('department'); ?>            						</th>
                                    					                                  						<th>
            							 <?php echo $this->Paginator->sort('location'); ?>            						</th>
                                    					                                  						<th>
            							 <?php echo $this->Paginator->sort('address'); ?>            						</th>
                                    					                                  						<th>
            							 <?php echo $this->Paginator->sort('phone_no'); ?>            						</th>
                                    					                                  						<th>
            							 <?php echo $this->Paginator->sort('membership_no'); ?>            						</th>
                                    					                                  						<th>
            							 <?php echo $this->Paginator->sort('old_form_no'); ?>            						</th>
                                    					                                  						<th>
            							 <?php echo $this->Paginator->sort('status'); ?>            						</th>
                                    					                                  						<th>
            							 <?php echo $this->Paginator->sort('not_active_date'); ?>            						</th>
                                    					                                  						<th>
            							 <?php echo $this->Paginator->sort('received_form'); ?>            						</th>
                                    					                                  						<th>
            							 <?php echo $this->Paginator->sort('remarks'); ?>            						</th>
                                    					                                  						<th>
            							 <?php echo $this->Paginator->sort('user_id'); ?>            						</th>
                                    					                                  					                                  						<th>
            							 <?php echo $this->Paginator->sort('updated'); ?>            						</th>
                                    					                        <th width="30"></th>
                      </tr>
                    </thead>
                    <tbody>
                      
                      <?php foreach ($members as $member): ?>
	<tr>
		<td class="actions">
			<?php echo $this->Html->link($this->Form->button('<i class="fa fa-desktop"></i>', array('class'=>'btn btn-xs btn-success', 'style'=>'color:#000; width:25px; height:25px;','data-toggle'=>'tooltip', 'data-placement'=>'right', 'title'=>'View Details')), array('action' => 'view', $member['Member']['id']), array('escape' => false)); ?>
			<?php echo $this->Html->link($this->Form->button('<i class="fa fa-pencil"></i>', array('class'=>'btn btn-xs btn-warning', 'style'=>'color:#000; width:25px; height:25px;','data-toggle'=>'tooltip', 'data-placement'=>'right', 'title'=>'Modify Details')), array('action' => 'edit', $member['Member']['id']), array('data-toggle'=>'ajaxModal', 'escape' => false)); ?>
		</td>
		<td>
			<?php echo $this->Html->link($member['Location']['name'], array('controller' => 'locations', 'action' => 'view', $member['Location']['id'])); ?>
		</td>
		<td><?php echo h($member['Member']['register_date']); ?>&nbsp;</td>
		<td><?php echo h($member['Member']['register_by']); ?>&nbsp;</td>
		<td><?php echo h($member['Member']['fullname']); ?>&nbsp;</td>
		<td><?php echo h($member['Member']['ic_no']); ?>&nbsp;</td>
		<td><?php echo h($member['Member']['staff_no']); ?>&nbsp;</td>
		<td><?php echo h($member['Member']['department']); ?>&nbsp;</td>
		<td><?php echo h($member['Member']['location']); ?>&nbsp;</td>
		<td><?php echo h($member['Member']['address']); ?>&nbsp;</td>
		<td><?php echo h($member['Member']['phone_no']); ?>&nbsp;</td>
		<td><?php echo h($member['Member']['membership_no']); ?>&nbsp;</td>
		<td><?php echo h($member['Member']['old_form_no']); ?>&nbsp;</td>
		<td><?php echo h($member['Member']['status']); ?>&nbsp;</td>
		<td><?php echo h($member['Member']['not_active_date']); ?>&nbsp;</td>
		<td><?php echo h($member['Member']['received_form']); ?>&nbsp;</td>
		<td><?php echo h($member['Member']['remarks']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($member['User']['name'], array('controller' => 'users', 'action' => 'view', $member['User']['id'])); ?>
		</td>
		<td><?php echo h($member['Member']['updated']); ?>&nbsp;</td>
	</tr>
<?php endforeach; ?>
						
                    </tbody>
                  </table>
                </div>
                <footer class="panel-footer">
                  <div class="row">
                    <div class="col-sm-4 text-left hidden-xs">
                      &nbsp; 

                    </div>
                    <div class="col-sm-4 text-center">
                      <small class="text-muted inline m-t-sm m-b-sm">
                     	<?php
									echo $this->Paginator->counter(array(
										'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
									));
									?>                      </small>
                    </div>
                    <div class="col-sm-4 text-right text-center-xs">                
                      <ul class="pagination pagination-sm m-t-none m-b-none">
                        
                        <?php
		echo '<li>'.$this->Paginator->first(__('first'), array(), null, array('class' => 'prev disabled')).'</li>';
		echo '<li>'.$this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled')).'</li>';
		echo '<li>'.$this->Paginator->numbers(array('separator' => '</li>
                        <li>')).'</li>';
		echo '<li>'.$this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled')).'</li>';
		echo '<li>'.$this->Paginator->last(__('last'), array(), null, array('class' => 'next disabled')).'</li>';
	?>
                      </ul>
                    </div>
                  </div>
                </footer>
              </section>

