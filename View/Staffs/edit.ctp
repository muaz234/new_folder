

<div class="modal-dialog">
  <div class="modal-content">
  	<?php echo $this->Form->create('Staff', array('type' => 'file', 'class' => 'bs-example form-horizontal')); ?>
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal">&times;</button>
      <h4 class="modal-title"><?php echo __('Edit Staff'); ?></h4>
    </div>
    <div class="modal-body">
				<?php
		echo $this->Form->input('id', array('class' => 'form-control'));
		echo $this->Form->input('staff_number', array('class' => 'form-control'));
		echo $this->Form->input('Staff_Name', array('class' => 'form-control'));
		echo $this->Form->input('Company', array('class' => 'form-control'));
		echo $this->Form->input('Depot', array('class' => 'form-control'));
		echo $this->Form->input('Working_Type', array('class' => 'form-control'));
		echo $this->Form->input('Group_Level', array('class' => 'form-control'));
		echo $this->Form->input('Employment_Status', array('class' => 'form-control'));
		echo $this->Form->input('Entry_Date', array('class' => 'form-control'));
		echo $this->Form->input('Seniority_Date', array('class' => 'form-control'));
		echo $this->Form->input('Leaving_Date', array('class' => 'form-control'));
		echo $this->Form->input('Years_of_Service', array('class' => 'form-control'));
		echo $this->Form->input('Organizational_Unit', array('class' => 'form-control'));
		echo $this->Form->input('Cost_Center', array('class' => 'form-control'));
		echo $this->Form->input('Cost_Center_Id', array('class' => 'form-control'));
		echo $this->Form->input('Designation', array('class' => 'form-control'));
		echo $this->Form->input('Division', array('class' => 'form-control'));
		echo $this->Form->input('Department', array('class' => 'form-control'));
		echo $this->Form->input('Section', array('class' => 'form-control'));
		echo $this->Form->input('Unit', array('class' => 'form-control'));
		echo $this->Form->input('Subunit', array('class' => 'form-control'));
		echo $this->Form->input('Staff_Grade', array('class' => 'form-control'));
		echo $this->Form->input('Gender', array('class' => 'form-control'));
		echo $this->Form->input('Date_of_Birth', array('class' => 'form-control'));
		echo $this->Form->input('Age', array('class' => 'form-control'));
		echo $this->Form->input('Remarks', array('class' => 'form-control'));
	?>
	</div>
    <div class="modal-footer">
    	<?php echo $this->Layout->sessionFlash(); ?>
    	<a href="#" class="btn btn-default" data-dismiss="modal">Close</a>
    	<?php echo $this->Form->button('Submit', array('class' => 'btn btn-primary'));?>
    </div>
    	<?php echo $this->Form->end();?>
  </div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->


