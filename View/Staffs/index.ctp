          <ul class="breadcrumb no-border no-radius b-b b-light pull-in">
            <li><a href="<?php echo $this->webroot; ?>"<i class="fa fa-home"></i> Home</a></li>
            <li class="active"><?php echo __('Staff'); ?> List</li>
          </ul>
          <div class="m-b-md">
            <h3 class="m-b-none"><?php echo __('Staff'); ?> List</h3>
          </div>

              <section class="panel panel-default">
                <div class="row wrapper">
                  <div class="col-sm-5 m-b-xs">
                      <div class="btn-group">
                        <button type="button" class="btn btn-sm btn-default" title="Refresh" onclick ='location.reload();'><i class="fa fa-refresh"></i></button>
                      </div>
                       			<?php echo $this->Html->link(' Create', array('action' => 'add'), array('class' => 'btn btn-success fa fa-plus', 'data-toggle'=>'ajaxModal', 'escape' => false)); ?>
 
                  </div>
                  <div class="col-sm-4 m-b-xs">
                    &nbsp;
                  </div>
                  <div class="col-sm-3">
                      <?php echo $this->Form->create('Staff', array('class' => 'bs-example form-horizontal', 'inputDefaults' => array('label' => false, 'div' => false))); ?>
                      <div class="input-group">
                      	<?php
		echo $this->Form->input('queryString', array('class' => 'input-sm form-control', 'placeholder' => 'Search')); ?>
<span class='input-group-btn'>
<?php echo $this->Form->button('Go!', array('class' => 'btn btn-sm btn-default bg-success'));?>
</span>
		</div><?php echo $this->Form->end();?>
                    </div>
                  </div>
                </div>
                <div class="table-responsive">
                  <table class="table table-striped b-t b-light">
                    <thead>
                      <tr>
                        
                        <th class="col-md-2 text-center sorting"><?php echo __('Actions'); ?></th>
                                                        					                                  						<th>
            							 <?php echo $this->Paginator->sort('staff_number'); ?>            						</th>
                                    					                                  						<th>
            							 <?php echo $this->Paginator->sort('Staff_Name'); ?>            						</th>
                                    					                                  						<th>
            							 <?php echo $this->Paginator->sort('Company'); ?>            						</th>
                                    					                                  						<th>
            							 <?php echo $this->Paginator->sort('Depot'); ?>            						</th>
                                    					                                  						<th>
            							 <?php echo $this->Paginator->sort('Working_Type'); ?>            						</th>
                                    					                                  						<th>
            							 <?php echo $this->Paginator->sort('Group_Level'); ?>            						</th>
                                    					                                  						<th>
            							 <?php echo $this->Paginator->sort('Employment_Status'); ?>            						</th>
                                    					                                  						<th>
            							 <?php echo $this->Paginator->sort('Entry_Date'); ?>            						</th>
                                    					                                  						<th>
            							 <?php echo $this->Paginator->sort('Seniority_Date'); ?>            						</th>
                                    					                                  						<th>
            							 <?php echo $this->Paginator->sort('Leaving_Date'); ?>            						</th>
                                    					                                  						<th>
            							 <?php echo $this->Paginator->sort('Years_of_Service'); ?>            						</th>
                                    					                                  						<th>
            							 <?php echo $this->Paginator->sort('Organizational_Unit'); ?>            						</th>
                                    					                                  						<th>
            							 <?php echo $this->Paginator->sort('Cost_Center'); ?>            						</th>
                                    					                                  						<th>
            							 <?php echo $this->Paginator->sort('Cost_Center_Id'); ?>            						</th>
                                    					                                  						<th>
            							 <?php echo $this->Paginator->sort('Designation'); ?>            						</th>
                                    					                                  						<th>
            							 <?php echo $this->Paginator->sort('Division'); ?>            						</th>
                                    					                                  						<th>
            							 <?php echo $this->Paginator->sort('Department'); ?>            						</th>
                                    					                                  						<th>
            							 <?php echo $this->Paginator->sort('Section'); ?>            						</th>
                                    					                                  						<th>
            							 <?php echo $this->Paginator->sort('Unit'); ?>            						</th>
                                    					                                  						<th>
            							 <?php echo $this->Paginator->sort('Subunit'); ?>            						</th>
                                    					                                  						<th>
            							 <?php echo $this->Paginator->sort('Staff_Grade'); ?>            						</th>
                                    					                                  						<th>
            							 <?php echo $this->Paginator->sort('Gender'); ?>            						</th>
                                    					                                  						<th>
            							 <?php echo $this->Paginator->sort('Date_of_Birth'); ?>            						</th>
                                    					                                  						<th>
            							 <?php echo $this->Paginator->sort('Age'); ?>            						</th>
                                    					                                  						<th>
            							 <?php echo $this->Paginator->sort('Remarks'); ?>            						</th>
                                    					                        <th width="30"></th>
                      </tr>
                    </thead>
                    <tbody>
                      
                      <?php foreach ($staffs as $staff): ?>
	<tr>
		<td class="actions">
			<?php echo $this->Html->link($this->Form->button('<i class="fa fa-desktop"></i>', array('class'=>'btn btn-xs btn-success', 'style'=>'color:#000; width:25px; height:25px;','data-toggle'=>'tooltip', 'data-placement'=>'right', 'title'=>'View Details')), array('action' => 'view', $staff['Staff']['id']), array('escape' => false)); ?>
			<?php echo $this->Html->link($this->Form->button('<i class="fa fa-pencil"></i>', array('class'=>'btn btn-xs btn-warning', 'style'=>'color:#000; width:25px; height:25px;','data-toggle'=>'tooltip', 'data-placement'=>'right', 'title'=>'Modify Details')), array('action' => 'edit', $staff['Staff']['id']), array('data-toggle'=>'ajaxModal', 'escape' => false)); ?>
		</td>
		<td><?php echo h($staff['Staff']['staff_number']); ?>&nbsp;</td>
		<td><?php echo h($staff['Staff']['Staff_Name']); ?>&nbsp;</td>
		<td><?php echo h($staff['Staff']['Company']); ?>&nbsp;</td>
		<td><?php echo h($staff['Staff']['Depot']); ?>&nbsp;</td>
		<td><?php echo h($staff['Staff']['Working_Type']); ?>&nbsp;</td>
		<td><?php echo h($staff['Staff']['Group_Level']); ?>&nbsp;</td>
		<td><?php echo h($staff['Staff']['Employment_Status']); ?>&nbsp;</td>
		<td><?php echo h($staff['Staff']['Entry_Date']); ?>&nbsp;</td>
		<td><?php echo h($staff['Staff']['Seniority_Date']); ?>&nbsp;</td>
		<td><?php echo h($staff['Staff']['Leaving_Date']); ?>&nbsp;</td>
		<td><?php echo h($staff['Staff']['Years_of_Service']); ?>&nbsp;</td>
		<td><?php echo h($staff['Staff']['Organizational_Unit']); ?>&nbsp;</td>
		<td><?php echo h($staff['Staff']['Cost_Center']); ?>&nbsp;</td>
		<td><?php echo h($staff['Staff']['Cost_Center_Id']); ?>&nbsp;</td>
		<td><?php echo h($staff['Staff']['Designation']); ?>&nbsp;</td>
		<td><?php echo h($staff['Staff']['Division']); ?>&nbsp;</td>
		<td><?php echo h($staff['Staff']['Department']); ?>&nbsp;</td>
		<td><?php echo h($staff['Staff']['Section']); ?>&nbsp;</td>
		<td><?php echo h($staff['Staff']['Unit']); ?>&nbsp;</td>
		<td><?php echo h($staff['Staff']['Subunit']); ?>&nbsp;</td>
		<td><?php echo h($staff['Staff']['Staff_Grade']); ?>&nbsp;</td>
		<td><?php echo h($staff['Staff']['Gender']); ?>&nbsp;</td>
		<td><?php echo h($staff['Staff']['Date_of_Birth']); ?>&nbsp;</td>
		<td><?php echo h($staff['Staff']['Age']); ?>&nbsp;</td>
		<td><?php echo h($staff['Staff']['Remarks']); ?>&nbsp;</td>
	</tr>
<?php endforeach; ?>
						
                    </tbody>
                  </table>
                </div>
                <footer class="panel-footer">
                  <div class="row">
                    <div class="col-sm-4 text-left hidden-xs">
                      &nbsp; 

                    </div>
                    <div class="col-sm-4 text-center">
                      <small class="text-muted inline m-t-sm m-b-sm">
                     	<?php
									echo $this->Paginator->counter(array(
										'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
									));
									?>                      </small>
                    </div>
                    <div class="col-sm-4 text-right text-center-xs">                
                      <ul class="pagination pagination-sm m-t-none m-b-none">
                        
                        <?php
		echo '<li>'.$this->Paginator->first(__('first'), array(), null, array('class' => 'prev disabled')).'</li>';
		echo '<li>'.$this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled')).'</li>';
		echo '<li>'.$this->Paginator->numbers(array('separator' => '</li>
                        <li>')).'</li>';
		echo '<li>'.$this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled')).'</li>';
		echo '<li>'.$this->Paginator->last(__('last'), array(), null, array('class' => 'next disabled')).'</li>';
	?>
                      </ul>
                    </div>
                  </div>
                </footer>
              </section>

