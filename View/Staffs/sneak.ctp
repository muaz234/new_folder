          <ul class="breadcrumb no-border no-radius b-b b-light pull-in">
            <li><a href="<?php echo $this->webroot; ?>"<i class="fa fa-home"></i> Home</a></li>
            <li class="active"><a href="#" onClick="location.reload();">Sneak Preview: <?php echo __('Staff'); ?>'s Details </a></li>
          </ul>
          <div class="m-b-md">
            <h3 class="m-b-none">Sneak Preview: <?php echo __('Staff'); ?>'s Details </h3>
          </div>
          <!-- start content view page -->
          <section class="vbox">
            <section class="scrollable">
              <section class="hbox stretch">
                <!-- start column 1 -->
                <aside class="aside-lg bg-light lter b-r">
                  <section class="vbox">
                    <section class="scrollable">
                      <div class="wrapper">
                        
                        <div class="portlet">
                          <section class="panel panel-info portlet-item">
                            <header class="panel-heading">
                            Information
                          </header>
                            		<small class="text-uc text-xs text-muted"><?php echo __('Id'); ?></small>
		<p>
			<?php echo h($staff['Staff']['id']); ?>
			&nbsp;
		</p>
		<small class="text-uc text-xs text-muted"><?php echo __('Staff Number'); ?></small>
		<p>
			<?php echo h($staff['Staff']['staff_number']); ?>
			&nbsp;
		</p>
		<small class="text-uc text-xs text-muted"><?php echo __('Staff Name'); ?></small>
		<p>
			<?php echo h($staff['Staff']['Staff_Name']); ?>
			&nbsp;
		</p>
		<small class="text-uc text-xs text-muted"><?php echo __('Company'); ?></small>
		<p>
			<?php echo h($staff['Staff']['Company']); ?>
			&nbsp;
		</p>
		<small class="text-uc text-xs text-muted"><?php echo __('Depot'); ?></small>
		<p>
			<?php echo h($staff['Staff']['Depot']); ?>
			&nbsp;
		</p>
		<small class="text-uc text-xs text-muted"><?php echo __('Working Type'); ?></small>
		<p>
			<?php echo h($staff['Staff']['Working_Type']); ?>
			&nbsp;
		</p>
		<small class="text-uc text-xs text-muted"><?php echo __('Group Level'); ?></small>
		<p>
			<?php echo h($staff['Staff']['Group_Level']); ?>
			&nbsp;
		</p>
		<small class="text-uc text-xs text-muted"><?php echo __('Employment Status'); ?></small>
		<p>
			<?php echo h($staff['Staff']['Employment_Status']); ?>
			&nbsp;
		</p>
		<small class="text-uc text-xs text-muted"><?php echo __('Entry Date'); ?></small>
		<p>
			<?php echo h($staff['Staff']['Entry_Date']); ?>
			&nbsp;
		</p>
		<small class="text-uc text-xs text-muted"><?php echo __('Seniority Date'); ?></small>
		<p>
			<?php echo h($staff['Staff']['Seniority_Date']); ?>
			&nbsp;
		</p>
		<small class="text-uc text-xs text-muted"><?php echo __('Leaving Date'); ?></small>
		<p>
			<?php echo h($staff['Staff']['Leaving_Date']); ?>
			&nbsp;
		</p>
		<small class="text-uc text-xs text-muted"><?php echo __('Years Of Service'); ?></small>
		<p>
			<?php echo h($staff['Staff']['Years_of_Service']); ?>
			&nbsp;
		</p>
		<small class="text-uc text-xs text-muted"><?php echo __('Organizational Unit'); ?></small>
		<p>
			<?php echo h($staff['Staff']['Organizational_Unit']); ?>
			&nbsp;
		</p>
		<small class="text-uc text-xs text-muted"><?php echo __('Cost Center'); ?></small>
		<p>
			<?php echo h($staff['Staff']['Cost_Center']); ?>
			&nbsp;
		</p>
		<small class="text-uc text-xs text-muted"><?php echo __('Cost Center Id'); ?></small>
		<p>
			<?php echo h($staff['Staff']['Cost_Center_Id']); ?>
			&nbsp;
		</p>
		<small class="text-uc text-xs text-muted"><?php echo __('Designation'); ?></small>
		<p>
			<?php echo h($staff['Staff']['Designation']); ?>
			&nbsp;
		</p>
		<small class="text-uc text-xs text-muted"><?php echo __('Division'); ?></small>
		<p>
			<?php echo h($staff['Staff']['Division']); ?>
			&nbsp;
		</p>
		<small class="text-uc text-xs text-muted"><?php echo __('Department'); ?></small>
		<p>
			<?php echo h($staff['Staff']['Department']); ?>
			&nbsp;
		</p>
		<small class="text-uc text-xs text-muted"><?php echo __('Section'); ?></small>
		<p>
			<?php echo h($staff['Staff']['Section']); ?>
			&nbsp;
		</p>
		<small class="text-uc text-xs text-muted"><?php echo __('Unit'); ?></small>
		<p>
			<?php echo h($staff['Staff']['Unit']); ?>
			&nbsp;
		</p>
		<small class="text-uc text-xs text-muted"><?php echo __('Subunit'); ?></small>
		<p>
			<?php echo h($staff['Staff']['Subunit']); ?>
			&nbsp;
		</p>
		<small class="text-uc text-xs text-muted"><?php echo __('Staff Grade'); ?></small>
		<p>
			<?php echo h($staff['Staff']['Staff_Grade']); ?>
			&nbsp;
		</p>
		<small class="text-uc text-xs text-muted"><?php echo __('Gender'); ?></small>
		<p>
			<?php echo h($staff['Staff']['Gender']); ?>
			&nbsp;
		</p>
		<small class="text-uc text-xs text-muted"><?php echo __('Date Of Birth'); ?></small>
		<p>
			<?php echo h($staff['Staff']['Date_of_Birth']); ?>
			&nbsp;
		</p>
		<small class="text-uc text-xs text-muted"><?php echo __('Age'); ?></small>
		<p>
			<?php echo h($staff['Staff']['Age']); ?>
			&nbsp;
		</p>
		<small class="text-uc text-xs text-muted"><?php echo __('Remarks'); ?></small>
		<p>
			<?php echo h($staff['Staff']['Remarks']); ?>
			&nbsp;
		</p>
                          <div class="line"></div>
                        </section>
                        <section class="panel panel-info portlet-item">
                          <header class="panel-heading">
                            <ul class="nav nav-pills pull-right">
                                      <li>
                                        <a href="#" class="panel-toggle text-muted"><i class="fa fa-caret-down text-active"></i><i class="fa fa-caret-up text"></i></a>
                                      </li>
                                    </ul>
                            Quick links
                          </header>
                          <div class="list-group bg-white">
                           <?php echo $this->Html->link(__('Edit Staff'), array('action' => 'edit', $staff['Staff']['id']), array('class'=>'list-group-item')); ?> <?php echo $this->Form->postLink(__('Delete Staff'), array('action' => 'delete', $staff['Staff']['id']), array('class'=>'list-group-item'), __('Are you sure you want to delete # %s?', $staff['Staff']['id'])); ?>  <?php echo $this->Html->link(__('List Staffs'), array('action' => 'index'), array('class'=>'list-group-item')); ?> <?php echo $this->Html->link(__('New Staff'), array('action' => 'add'), array('class'=>'list-group-item')); ?>                          </div>
                        </section>
                        <section class="panel panel-info portlet-item">
                          <header class="panel-heading">
                            <ul class="nav nav-pills pull-right">
                                      <li>
                                        <a href="#" class="panel-toggle text-muted"><i class="fa fa-caret-down text-active"></i><i class="fa fa-caret-up text"></i></a>
                                      </li>
                                    </ul>
                            Associated links
                          </header>
                          <div class="list-group bg-white">
                                                    </div>
                        </section>
                        </div>
                      </div>
                    </section>
                  </section>
                </aside>
                <!-- end column 1 -->
                
              </section>
            </section>
          </section>
          <!-- end content view page -->