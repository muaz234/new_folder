          <ul class="breadcrumb no-border no-radius b-b b-light pull-in">
            <li><a href="<?php echo $this->webroot; ?>"<i class="fa fa-home"></i> Home</a></li>
            <li class="active"><a href="#" onClick="location.reload();"><?php echo __('Welfare Type'); ?>'s Details</a></li>
          </ul>
          <div class="m-b-md">
            <h3 class="m-b-none"><?php echo __('Welfare Type'); ?>'s Details</h3>
          </div>
          <!-- start content view page -->
          <section class="vbox">
            <section class="scrollable">
              <section class="hbox stretch">
                <!-- start column 1 -->
                <aside class="aside-lg bg-light lter b-r">
                  <section class="vbox">
                    <section class="scrollable wrapper">
                      <!-- portlet -->
                      &nbsp;
                      <!-- end portlet -->
                    </section>
                  </section>
                </aside>
                <!-- end column 1 -->
                <!-- start column 2 -->
                                <aside class="bg-white">
                  <section class="vbox">
                    <!-- tab content headers -->
                    <header class="header bg-light bg-gradient" style="height:auto">
                      <div>
                        <ul class="nav nav-tabs nav-white">
                        <!-- start tab link -->

                        <li class="active"><a href="#dashboard" data-toggle="tab">Dashboard</a></li>
                                                  <!-- end of hasOne -->
                          <!-- start hasMany -->
                                                      <li><a href="#Welfares" data-toggle="tab">Welfares</a></li>
                            
                          <!-- end hasMany -->
                        <!-- end tab link -->
                        </ul>
                      </div>
                    </header>
                    <!-- end tab content headers -->
                    <!-- tab content article-->
                    <article class="scrollable">
                      <!-- tab content div -->
                      <div class="tab-content">
                        <!-- start tab content -->
                        <div class="tab-pane padder active" id="dashboard">
                            <h3><?php echo __('Information'); ?></h3>
                            <!-- <section class="panel panel-default padder"> -->
                              <section class="panel panel-info portlet-item">
                                <header class="panel-heading">
                                Information
                                </header>
                                		<small class="text-uc text-xs text-muted"><?php echo __('Id'); ?></small>
		<p>
			<?php echo h($welfareType['WelfareType']['id']); ?>
			&nbsp;
		</p>
		<small class="text-uc text-xs text-muted"><?php echo __('Name'); ?></small>
		<p>
			<?php echo h($welfareType['WelfareType']['name']); ?>
			&nbsp;
		</p>
		<small class="text-uc text-xs text-muted"><?php echo __('User'); ?></small>
		<p>
			<?php echo $this->Html->link($welfareType['User']['name'], array('controller' => 'users', 'action' => 'view', $welfareType['User']['id'])); ?>
			&nbsp;
		</p>
		<small class="text-uc text-xs text-muted"><?php echo __('Status'); ?></small>
		<p>
			<?php echo h($welfareType['WelfareType']['status']); ?>
			&nbsp;
		</p>
		<small class="text-uc text-xs text-muted"><?php echo __('Updated'); ?></small>
		<p>
			<?php echo h($welfareType['WelfareType']['updated']); ?>
			&nbsp;
		</p>
                              </section>
                              <section class="panel panel-info portlet-item">
                                <header class="panel-heading">
                                  Quick links
                                </header>
                                <div class="list-group bg-white">
                                 <?php echo $this->Html->link(__('Edit Welfare Type'), array('action' => 'edit', $welfareType['WelfareType']['id']), array('class'=>'list-group-item', 'data-toggle'=>'ajaxModal')); ?> <?php echo $this->Form->postLink(__('Delete Welfare Type'), array('action' => 'delete', $welfareType['WelfareType']['id']), array('class'=>'list-group-item'), __('Are you sure you want to delete # %s?', $welfareType['WelfareType']['id'])); ?>  <?php echo $this->Html->link(__('List Welfare Types'), array('action' => 'index'), array('class'=>'list-group-item')); ?> <?php echo $this->Html->link(__('New Welfare Type'), array('action' => 'add'), array('class'=>'list-group-item', 'data-toggle'=>'ajaxModal', 'escape' => false)); ?>                                    </div>
                              </section>
                            <!-- </section> -->
                        </div>
                        <!-- start hasOne -->
                                                  <!-- end hasOne -->
                          <!-- start hasMany -->
                                                  <div class="tab-pane padder" id="Welfares">
                            <h3><?php echo __('Related Welfares'); ?></h3>
                            

                            <?php if (!empty($welfareType['Welfare'])): ?>
                            <section class="panel panel-default padder">
                              <div class="row wrapper">
                                <div class="col-sm-5 m-b-xs">
                                    			<?php echo $this->Html->link(' Create', array('controller' => 'welfares', 'action' => 'add', '?' => array('returnURL' => $this->Html->url( null, true ))), array('class' => 'btn btn-default bg-success fa fa-plus', 'data-toggle'=>'ajaxModal', 'style'=>'color:#000;', 'escape' => false)); ?>
 
                                </div>
                              </div>
                              <div class="table-responsive">
                                <table class="table table-striped b-t b-light">
                                  <thead>
                                    <tr>
                                      <th class="actions"><?php echo __('Actions'); ?></th>
                                                                                                                                                                                              <th>
                                        		<?php echo __('User Id'); ?>
                                      </th>
                                                                                                                                                      <th>
                                        		<?php echo __('Member Id'); ?>
                                      </th>
                                                                                                                                                      <th>
                                        		<?php echo __('Welfare Type Id'); ?>
                                      </th>
                                                                                                                                                      <th>
                                        		<?php echo __('Approved On'); ?>
                                      </th>
                                                                                                                                                      <th>
                                        		<?php echo __('Issuance Date'); ?>
                                      </th>
                                                                                                                                                                                                                                  <th>
                                        		<?php echo __('Updated'); ?>
                                      </th>
                                                                                                              </tr>
                                  </thead>
                                  <tfoot>
                                    <tr>
                                      <th class="actions"><?php echo __('Actions'); ?></th>
                                                                                                                                                                                              
                                        		<th><?php echo __('User Id'); ?></th>
                                      
                                                                                                                                                      
                                        		<th><?php echo __('Member Id'); ?></th>
                                      
                                                                                                                                                      
                                        		<th><?php echo __('Welfare Type Id'); ?></th>
                                      
                                                                                                                                                      
                                        		<th><?php echo __('Approved On'); ?></th>
                                      
                                                                                                                                                      
                                        		<th><?php echo __('Issuance Date'); ?></th>
                                      
                                                                                                                                                                                                                                  
                                        		<th><?php echo __('Updated'); ?></th>
                                      
                                                                                                              </tr>
                                  </tfoot>
                                  
                                  <tbody>
                                  	<?php foreach ($welfareType['Welfare'] as $welfare): ?>
		<tr>
			<td class="actions">
				<?php echo $this->Html->link('', array('controller' => 'welfares', 'action' => 'sneak', $welfare['id']), array('class'=>'btn btn-xs btn-success fa fa-desktop', 'style'=>'color:#000;width:25px; height:25px; padding-top:5px','data-toggle'=>'tooltip', 'data-placement'=>'right', 'title'=>'View Details', 'data-toggle'=>'ajaxModal', 'escape' => false)); ?>
				<?php echo $this->Html->link('', array('controller' => 'welfares', 'action' => 'edit', $welfare['id']), array('class'=>'btn btn-xs btn-warning fa fa-pencil', 'style'=>'color:#000;width:25px; height:25px; padding-top:5px','data-toggle'=>'tooltip', 'data-placement'=>'right', 'title'=>'Modify Details', 'data-toggle'=>'ajaxModal', 'escape' => false)); ?>
			</td>
			<td><?php echo $welfare['user_id']; ?></td>
			<td><?php echo $welfare['member_id']; ?></td>
			<td><?php echo $welfare['welfare_type_id']; ?></td>
			<td><?php echo $welfare['approved_on']; ?></td>
			<td><?php echo $welfare['issuance_date']; ?></td>
			<td><?php echo $welfare['updated']; ?></td>
		</tr>
	<?php endforeach; ?>
                                    <tr>
                                    </tr>
                                  </tbody>
                                </table>
                              </div>
                            <?php endif; ?>

                            <br/>
                            <div class="actions">
                              <div class="col-sm-5 m-b-xs">
                                  			<?php echo $this->Html->link(' Create', array('controller' => 'welfares', 'action' => 'add', '?' => array('returnURL' => $this->Html->url( null, true ))), array('class' => 'btn btn-default bg-success fa fa-plus', 'data-toggle'=>'ajaxModal', 'style'=>'color:#000;', 'escape' => false)); ?>
 
                              </div>
                            </div>
                          </div>
                                                    <!-- end hasMany -->
                        <!-- end tab content -->
                      </div><!-- end tab content div -->
                    </article> <!-- end tab content article-->
                  </section>
                </aside>
                                <!-- end column 2 -->
              </section>
            </section>
          </section>
          <!-- end content view page -->

